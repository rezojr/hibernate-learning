package org.jpwh.test.advanced;

import org.hibernate.Session;
import org.hibernate.engine.jdbc.StreamUtils;
import org.jpwh.env.JPATest;
import org.jpwh.model.advanced.Item;
import org.testng.annotations.Test;

import javax.imageio.stream.ImageInputStream;
import javax.persistence.EntityManager;
import javax.persistence.Temporal;
import javax.transaction.UserTransaction;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.util.Random;

import static org.testng.Assert.assertEquals;

public class LazyProperties extends JPATest {

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("AdvancedPU");
    }

    @Test
    public void storeLoadProperties() throws Exception {
        UserTransaction tx = TM.getUserTransaction();

        try{
            tx.begin();
            EntityManager em = JPA.createEntityManager();
            Item someItem = new Item();
            someItem.setName("Some item");
            someItem.setDescription("This is some description");
            byte[] bytes = new byte[131072];
            new Random().nextBytes(bytes);
            someItem.setImage(bytes);
            em.persist(someItem);
            tx.commit();
            em.close();

            Long ITEM_ID = someItem.getId();

            tx.begin();
            em = JPA.createEntityManager();

            Item item = em.find(Item.class, ITEM_ID);

            assertEquals(item.getDescription(), "This is some description");
            assertEquals(item.getImage().length, 131072);

            tx.commit();
            em.close();
        } finally {
            TM.rollback();
        }
    }

    @Test
    public void storeLoadLocator() throws Exception {
        UserTransaction tx = TM.getUserTransaction();
        try {
            tx.begin();
            EntityManager em = JPA.createEntityManager();

            byte[] bytes = new byte[131072];
            new Random().nextBytes(bytes);
            InputStream imageInputStream = new ByteArrayInputStream(bytes);
            int byteLength = bytes.length;

            Item someItem = new Item();
            someItem.setName("Some item");
            someItem.setDescription("This is some description");

            Session session = em.unwrap(Session.class);
            Blob blob = session.getLobHelper()
                    .createBlob(imageInputStream, byteLength);

            someItem.setImageBlob(blob);
            em.persist(someItem);

            tx.commit();
            em.close();

            Long ITEM_ID = someItem.getId();

            tx.begin();
            em = JPA.createEntityManager();

            Item item = em.find(Item.class, ITEM_ID);

            InputStream imageDataStream = item.getImageBlob().getBinaryStream();

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            StreamUtils.copy(imageDataStream, outputStream);
            byte[] imageBytes = outputStream.toByteArray();
            assertEquals(imageBytes.length, 131072);

            tx.commit();
            em.close();

        } finally {
            TM.rollback();
        }
    }

}
