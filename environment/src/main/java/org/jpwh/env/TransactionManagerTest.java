package org.jpwh.env;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.util.Locale;

public class TransactionManagerTest {
    public static TransactionManagerSetup TM;

    @Parameters({"database", "connectionURL"})
    @BeforeSuite
    public void beforeSuite(@Optional String database,
                            @Optional String connection) throws Exception{
        TM = new TransactionManagerSetup(
                database != null
                ? DatabaseProduct.valueOf(database.toUpperCase(Locale.US))
                        : DatabaseProduct.H2,
                connection
        );
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuite() throws Exception {
        if (TM != null)
            TM.stop();
    }

}
