package org.jpwh.model.advanced.converter;

import org.jpwh.converter.ZipcodeConverter;
import org.jpwh.model.simple.Address;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "USERS")
public class User implements Serializable {

    @Convert(
            converter = ZipcodeConverter.class,
            attributeName = "zipcode"
    )
    protected Address homeAddress;

}
