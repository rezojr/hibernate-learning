package org.jpwh.model.advanced.converter;

import org.jpwh.model.advanced.MonetaryAmount;
import org.jpwh.model.coverter.MonetaryAmountConverter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class Item {

    @Id
    @GeneratedValue(generator = "ID_GENERATOR")
    protected Long id;

    @NotNull
    protected String name;

    @NotNull
    @Convert(
            converter = MonetaryAmountConverter.class,
            disableConversion = false)
    @Column(name = "PRICE", length = 63)
    protected MonetaryAmount buyNowPrice;

    @NotNull
    protected Date createdOn = new Date();

}
